import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { HttpClientModule } from '@angular/common/http';
import { StoriesModule } from './stories/stories.module';
import { HomeModule } from '@components/home/home.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
	AppComponent
  ],
  imports: [
	HttpClientModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
	CommonModule,
	ServiceWorkerModule.register('ngsw-worker.js', {
	enabled: environment.production,
		// Register the ServiceWorker as soon as the application is stable
		// or after 30 seconds (whichever comes first).
		// registrationStrategy: 'registerWhenStable:30000'
		registrationStrategy: 'registerImmediately'
	}),
	StoriesModule,
	HomeModule,
    AppRoutingModule
  ],
  schemas: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
