import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
	{
		path: '',
		loadChildren: () => import('./components/home/home.module').then(m => m.HomeModule),
	},
	{
		path: 'stories',
		loadChildren: () => import('./stories/stories.module').then(m => m.StoriesModule),
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			initialNavigation: 'enabledBlocking'
		}),
	],
	exports: [RouterModule]
})
export class AppRoutingModule { }
