import IPixelMapDay from "@interfaces/pixel-map-day.interface";

const januaryMap: Array<IPixelMapDay> = [
	{
		status: "excellent",
		icon: "flare",
		title: "New Year's Day",
		reason: "Prepared a gift idea. Had some time alone in a park to think about things and last year. Claimed Starbucks cold cup from sticker set.",
		spotify: {
			artist: "NewJeans",
			song: "Ditto",
			link: "https://open.spotify.com/track/3r8RuvgbX9s7ammBn07D3W?si=369ee558db4a42fa",
			album: "Ditto"
		}
	}, {
		status: "above-average"
	}, {
		status: "bad",
		icon: "sports_esports",
		reason: "Had a fight with a close, special person. They had a bad day, too, so that kind of snowballed onto me. We were okay within the same night, though.",
		spotify: {
			artist: "PRISTIN",
			album: "The 1st Mini Album 'HI! PRISTIN'",
			song: "WEE WOO",
			link: "https://open.spotify.com/track/23XXMK9SQBFwndnbgbcMPa?si=92e34c061f6e45df"
		}
	}, {
		status: "average"
	}, {
		status: "good",
		icon: "music_note",
		reason: "I finally watched New Jeans' new music video! It was the song to loop ever since I first heard of it. I didn't really have a lot of stress at work and I had fun with my friends on Discord (town hall).",
		spotify: {
			artist: "NewJeans",
			song: "OMG",
			link: "https://open.spotify.com/track/65FftemJ1DbbZ45DUfHJXE?si=078c8b7e09214fdf",
			album: "NewJeans 'OMG'"
		}
	}, {
		status: "bad",
		icon: "liquor",
		reason: "I went out with friends and then I got scolded by somebody. I wasn't there when they needed me. It felt like things were going to reset (once again) and I just wiped my entire Discord profile. I'm so scared of that \"1 mistake that changes everything\" from last year. Since I was with friends, I drank a lot with them."
	}, {
		status: "below-average",
		icon: "ramen_dining",
		reason: "Met up with friends at BGC and had some ramen. This would be my second-to-the-last time I would see my best friend before he leaves for the military. I was still in pain/shock from the day prior."
	}, {
		status: "excellent",
		icon: "movie",
		title: "First Friends Outing Day",
		reason: "Had some fun alone time, teasing each other and making jokes. Watched movies with 1 group of friends, spent the evening with another group of friends. Said goodbye to my best friend and met their family again after years."
	}, {
		status: "above-average",
		icon: "call",
		spotify: {
			artist: "LE SSERAFIM",
			song: "Antifragile",
			link: "https://open.spotify.com/track/4fsQ0K37TOXa3hEQfjEic1?si=2f2d1e311fd0476c",
			album: "ANTIFRAGILE",
		}
	}, {
		status: "excellent",
		icon: "heart_plus",
		spotify: {
			artist: "PRISTIN",
			album: "The 1st Mini Album 'HI! PRISTIN'",
			song: "WEE WOO",
			link: "https://open.spotify.com/track/23XXMK9SQBFwndnbgbcMPa?si=92e34c061f6e45df"
		}
	}, {
		status: "average",
		reason: "Got through so many hard tasks. Stressed out at writing SQL queries. Something felt off today but I can't explain what. Maybe it's because someone was mostly gone for the day. I watched Back to the Future, though.",
		spotify: {
			song: "Sour Grapes",
			artist: "LE SSERAFIM",
			link: "https://open.spotify.com/track/6wBpO4Xc4YgShnENGSFA1M?si=935545a899834f75",
			album: "FEARLESS"
		}
	}, {
		status: "average",
		reason: "Ate KFC and watched a few things like Back to the Future 2, Click, Single's Inferno S2, and Interstellar. Had a tough time trying to regulate my thoughts and feelings, but pushed through with no hitches.",
		spotify: {
			artist: "aespa",
			song: "Life's Too Short",
			link: "https://open.spotify.com/track/2mgzUVvDpb1zMSB4glLQ6T?si=e880a13dcfd745d0",
			album: "Life's Too Short"
		}
	}, {
		status: "above-average",
		reason: "I seem to have done a good job with my tasks. I talked to a lot of my friends online today."
	}, {
		status: "good",
		icon: "star_half",
		reason: "I was added to the \"Close Friends\" list on IG by a friend!!! Also, I was on call with a few friends today, friends who I haven't talked to in a while. I had an extreme bout of anxiety when I remembered Feb 12, 2022, but I was able to channel it by redesigning the popup modal for this calendar.",
		spotify: {
			artist: "NewJeans",
			song: "OMG",
			link: "https://open.spotify.com/track/65FftemJ1DbbZ45DUfHJXE?si=078c8b7e09214fdf",
			album: "NewJeans 'OMG'"
		}
	}, {
		status: "awful",
		icon: "heart_minus",
		title: "Here, In...",
		reason: "Pains from yesterday and yesteryear proved too much for me. I had doubts, anxiety, and extreme worries that kept me up all night early this day. I was also extremely stressed and physically stretched. The night kind of turned it around but the general mood was still awful.",
		spotify: {
			artist: "Bruno Major",
			song: "Regent's Park",
			link: "https://open.spotify.com/track/2qiDXgq5fL1FetkhX5xi9L?si=66ba7e955b4f4b82",
			album: "To Let A Good Thing Die"
		}
	}, {
		status: "good",
		reason: "After clearing up a lot of things from yesterday and learning how to be a bit more honest, things felt light today. I was on call for most of the day and played Stardew Valley.",
		spotify: {
			artist: "aespa",
			song: "Girls",
			link: "https://open.spotify.com/track/2WTHLEVjfefbGoW7F3dXIg?si=b81c052c8ecc4b3b",
			album: "Girls"
		}
	}, {
		status: "good",
		icon: "cake",
		reason: "It was a fun birthday, I guess. It's not really \"special\" and I had some problems, per usual. There are rough days like this where things are a little bit harder to get through, but I'm generally okay.",
	}, {
		status: "below-average",
		reason: "Had several arguments today.",
	}, {
		status: "above-average",
	}, {
		status: "bad",
		icon: "event",
		title: "Beginning of the End ..?",
		reason: "This day marks the wait until April. I'm not happy for either choice, to be honest. My trust is really broken and I 100% expect them to hide and not be upfront. I highly doubt it, and they won't. Inb4 they'll pull the \"You didn't ask so I didn't tell\" card in a few months. We had agreed to have things straightened between us by April, a decision has to be made. I need to prepare for April, no matter what might happen.",
		spotify: {
			artist: "Hoàng Thùy Linh",
			album: "See Tình (Speed Up Version)",
			song: "See Tình (Speed Up Version)",
			link: "https://open.spotify.com/track/5sCATdnR2mWbHgdmIXBFHa?si=9d6f9fd1fbdd44d8"
		}
	}, {
		status: "awful",
		icon: "sick",
		reason: "I had a horrible headache, and there were a lot of inconsistencies. Power imbalance and unfairness. Why can they? Why can't I? Why? I'm so sick of the unfairness.",
		spotify: {
			artist: "Various Artists",
			song: "Wadde Hadde Dudde Da? (2000)",
			link: "https://open.spotify.com/track/455Mgke2AOROfEcIqHeY8X?si=16409ebabc374bc2",
			album: "24 Grand Prix Hits"
		}
	}, {
		status: "above-average",
		icon: "cake_add",
		reason: "I felt limited by the time I had to meet my friends. Max gave (BAKED!!!) me my favorite type of cake and Meryll gave me 2 K-Pop shirts! We hung out and I played NewJeans songs on the massage chair. But I met my friends, I played great music with them, and they were there for me. I appreciate them for staying and putting in the effort.",
		spotify: {
			artist: "NewJeans",
			song: "OMG",
			link: "https://open.spotify.com/track/65FftemJ1DbbZ45DUfHJXE?si=078c8b7e09214fdf",
			album: "NewJeans 'OMG'"
		}
	}, {
		status: "bad",
		reason: "The day was actually pretty good. It was the evening that really ruined it.",
	}, {
		status: "below-average",
		reason: "We doubled down on our stances. I didn't feel okay for most of the day. I talked with Rey to help me through this. We somewhat resolved it at the end of the day but the whole situation still has me uneasy. I wonder what we can do for the both of us to actually fix our situation.",
	}, {
		status: "above-average",
		reason: "Today was pretty good, we coordinated well.",
		spotify: {
			artist: "ABBA",
			song: "Waterloo",
			link: "https://open.spotify.com/track/3Dy4REq8O09IlgiwuHQ3sk?si=d65f5374b1f142a4",
			album: "Waterloo"
		}
	}, {
		status: "above-average",
		reason: "We made Bondee accounts and stayed on call for most of the day (what's new).",
	}, {
		status: "above-average",
	}, {
		status: "awful",
		icon: "heart_broken",
		reason: "Something feels off, super off.",
	}, {
		status: "bad",
		reason: "I've just realized that I was staying with the wrong person all this time. I wrote about it.",
		spotify: {
			artist: "The Walters",
			song: "I Love You So",
			link: "https://open.spotify.com/track/4SqWKzw0CbA05TGszDgMlc?si=73e9ca009de544f7",
			album: "I Love You So"
		}
	}, {
		status: "good",
		reason: "Things returned to their usual place today. We had a good moment to look back at things and critically look at how things have been between us, both in the past and in the present. They kept me company all throughout the evening.",
	}, {
		status: "above-average",
		icon: "image",
		spotify: {
			artist: "MRLD",
			song: "An Art Gallery Could Never Be As Unique As You",
			link: "https://open.spotify.com/track/22Nd3GuO7sHopPjdKccRcq?si=105af160fcab46c4",
			album: "An Art Gallery Could Never Be As Unique As You"
		}
	}
];

export default januaryMap;
