import IPixelMapDay from "@interfaces/pixel-map-day.interface";

const aprilMap: Array<IPixelMapDay> = [
    {
		status: "above-average",
		reason: "It was a mostly-normal Saturday. We had a small argument, but things turned out okay. I was able to open up about a really major secret to them. Things were okay, thankfully.",
		spotify: {
			song: "Cupid - Twin Ver.",
			artist: "FIFTY FIFTY",
			link: "https://open.spotify.com/track/3Kw7zkALCVxY4wmlnh2IWC?si=56f9c55cc7aa4005",
			album: "The Beginning: Cupid"
		}
	},
    {
		status: "bad",
		icon: "close",
		reason: "I met with someone today. We had an \"okay\" day out, but it didn't live up to my expectations.",
		spotify: {
			artist: "sped up 8282",
			album: "Cupid – Twin Ver. (FIFTY FIFTY) – Sped Up Version",
			song: "Cupid – Twin Ver. (FIFTY FIFTY) – Sped Up Version",
			link: "https://open.spotify.com/track/3B228N0GxfUCwPyfNcJxps?si=00fb3ea3ee5d43db"
		}
	},
	{
		status: "awful",
		icon: "speed",
		title: "Best Isn’t Good Enough",
		reason: "Maybe bringing my \"A game\" would be enough to tackle all of my problems. But no."
	},
	{
		status: "bad",
		reason: "I met someone new and they're kinda cool. I was supposed to take a nap, but I couldn't. I'm not sure what to do now."
	},
	{
		status: "below-average",
		reason: "She noticed that something was off with me and I talked to her about whether or not I should move on. She didn't want to let go and we had an okay-enough day together. But still, things were in shambles.",
		spotify: {
			song: "You Can Call Me Al",
			artist: "Paul Simon",
			link: "https://open.spotify.com/track/0qxYx4F3vm1AOnfux6dDxP?si=69f3bddaefa442c0",
			album: "Graceland (25th Anniversary Deluxe Edition)"
		}
	},
	{
		status: "below-average",
		reason: "I think I know what to do now."
	},
	{
		status: "bad",
		reason: "She ended our conversation by agreeing to just talk tomorrow.",
		spotify: {
			song: "Am I Wrong",
			artist: "Nico & Vinz",
			link: "https://open.spotify.com/track/1fidCEsYlaVE3pHwKCvpFZ?si=e32a313ff65e48a6",
			album: "Black Star Elephant"
		}
	},
	{
		status: "below-average",
		reason: "Resolving our conflict from last night was okay. She felt bad and I felt bad, too. So I stayed at a coffee shop and we just talked. We both resolved things in a way today, so I think we're okay."
	},
	{
		status: "special",
		icon: "theaters",
		title: "Finally Finished a Show",
		reason: "We finally finished an anime series today! It was so fun.",
		spotify: {
			artist: "NewJeans",
			song: "OMG",
			link: "https://open.spotify.com/track/65FftemJ1DbbZ45DUfHJXE?si=078c8b7e09214fdf",
			album: "NewJeans 'OMG'"
		}
	},
	{
		status: "above-average",
		reason: "Things were a fairly-normal day for us, until in the afternoon where she had a fight with her parents. Call was still fun in the evening because I tried to help her set up an IG account and she sent cute poses. She also cut her bangs during call as well."
	},
	{
		status: "excellent",
		icon: "celebration",
		title: "Calling!",
		reason: "Call was fun today and I realized how fun it was just to spend time with them.",
		spotify: {
			artist: "Taylor Swift",
			album: "Lover",
			song: "Paper Rings",
			link: "https://open.spotify.com/track/4y5bvROuBDPr5fuwXbIBZR?si=8e0decec947742d6"
		}
	},
	{
		status: "above-average",
		reason: "We both got doughnuts for lunch and ate together while watching Gravity Falls. She introduced me to AKMU and spent the day watching K-Pop videos."
	},
	{
		status: "above-average",
		reason: "She was leaning into her old favorite boy group again, VIXX, and she's so happy and sad about them. I'm trying my best to keep her company during these times and I hope I'm doing well. I also told her something and I'm surprised she was okay with it."
	},
	{
		status: "above-average",
		reason: "We spent most of the day together and watched The Wonder in the evening. I think I overate today.",
		spotify: {
			song: "Blue Flame",
			artist: "LE SSERAFIM",
			link: "https://open.spotify.com/track/37YoRLUu1qId0ewavgvnkG?si=78eeda61be7f4220",
			album: "FEARLESS"
		}
	},
	{
		status: "above-average",
		reason: "It was a mostly-fun day alone.",
		spotify: {
			song: "2002",
			album: "Speak Your Mind (Deluxe)",
			artist: "Anne-Marie",
			link: "https://open.spotify.com/track/2BgEsaKNfHUdlh97KmvFyo?si=545762fecca54802"
		}
	},
	{
		status: "above-average",
		reason: "We played Overcooked 2 again and this time it was just us. I also sold my Sennheiser HD 800S headphones today. I'm a bit distraught about it, but I promise my future self that I would buy myself a new pair when my living conditions improve. We also watched Ghostwatch today and it really scared the both of us despite us knowing that it was staged."
	},
	{
		status: "good",
		reason: "I bought Red Dead Redemption and she watched me stream it while we had shared boba tea after work."
	},
	{
		status: "bad",
		reason: "A fight."
	},
	{
		status: "excellent",
		icon: "mic_external_on",
		title: "Kai-raoke!",
		reason: "Today was a rough day, I'm still shaken from yesterday's sudden fight. I still hung out with a friend named Kai and we met for the first time. It was fun even though it was hot outside. She was chill to be with and we had a lot of fun in the karaoke booth. She gave me that perfume she promised and blueberry jam, which made my day. I came home expecting the worst and it was one of the worst fights I've had this year. I'm not letting that fight break down my day.",
		spotify: {
			artist: "Join The Club",
			album: "Nobela",
			song: "Nobela",
			link: "https://open.spotify.com/track/3vGkyQlCw8LklNip0ZUELC?si=3fdda0e080cb4a9d"
		}
	},
	{
		status: "good",
		reason: "We played Overcooked 2 and played Keep Talking and Nobody Explodes while drunk. We were both tense from last night's fight and she even took a small break after calling.",
		spotify: {
			song: "Night Changes",
			album: "FOUR (Deluxe)",
			artist: "One Direction",
			link: "https://open.spotify.com/track/5O2P9iiztwhomNh8xkR9lJ?si=b52f36a073944ced"
		}
	},
	{
		status: "above-average",
		reason: "It was a chill day at home. I played Apex Legends alone and it made me remember the time period I had in 2021, playing Apex with the boys, hanging out, and talking about life or other things. I watched MVs in the evening."
	},
	{
		status: "good",
		icon: "menu_book",
		reason: "Someone and I went out today. I can sense that they're tense from going out with me.",
		spotify: {
			song: "Beautiful Christmas",
			album: "Beautiful Christmas",
			artist: "Red Velvet",
			link: "https://open.spotify.com/track/3k7FTBQkstaBcYHamx9jqe?si=f742c91cffb14496"
		}
	},
	{
		status: "above-average",
		reason: "I got my SIM cards registered and I didn't feel okay, so I had to get a haircut outside. Getting a new haircut was okay. I watched The Handmaid's Tale and then called Kai. I was only going to call her for a bit but she was fun to talk to so the call extended for hours."
	},
	{
		status: "average",
		reason: "SEVENTEEN released an album today!",
		spotify: {
			artist: "SEVENTEEN",
			album: "SEVENTEEN 10th Mini Album 'FML'",
			song: "F*ck My Life",
			link: "https://open.spotify.com/track/3tgWMPOY4stCdKYj5NjrAe?si=4d3cc388b1404286"
		}
	},
	{
		status: "average",
		reason: "I stood up late with someone."
	},
	{
		status: "good",
		icon: "update",
		title: "It's Been YEARS!",
		reason: "I met with some old friends I haven't seen in a long time. I missed them and I realized how mature they already are in their lives. It's been years and I can't believe I saw them again post-pandemic."
	},
	{
		status: "average",
		reason: "I redesigned the hero banner of my website to feel productive and it was good to put some time into building and design."
	},
	{
		status: "below-average",
		reason: "I feel like I overspent and overate today, honestly."
	},
	{
		status: "excellent",
		icon: "ramen_dining",
		title: "Gateway Shenanigans",
		reason: "I went out today! I honestly didn't expect that we would change locations at the last minute, but we still had fun and things were easy-going between us. Things felt oddly easy which made me happy because I'm always anxious about things. We went to a ramen shop far out and took a lot of pics together.",
		spotify: {
			artist: "KC & The Sunshine Band",
			album: "All In a Night's Work (Expanded Version)",
			song: "Give It Up - 12\" Version",
			link: "https://open.spotify.com/track/0vMeLka5FpPTdBZFi803d0?si=f3f9ed1a2b5541af"
		}
	},
	{
		status: "average",
	},
];

export default aprilMap;
