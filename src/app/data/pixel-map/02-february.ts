import IPixelMapDay from "@interfaces/pixel-map-day.interface";

const februaryMap: Array<IPixelMapDay> = [
    {
		status: "awful",
		icon: "directions_run",
		reason: "It was just me stewing in my thoughts. I had 4 people leave me in January. It's still sad. Someone tried to cheer me up today, and another friend got me milk tea, but I was still generally gloomy.",
	}, {
		status: "special",
		icon: "forum",
		title: "Communication Is Key",
		reason: "I told them everything I can today. I sent them 2 letters about everything I felt and how my situation is like. And... talking worked???? We both came to a realization about the nature of our relationship and things between us were so much clearer. What they said at the start was true: \"You don't have to try hard in helping this 'relationship' grow since I am there with you. Here's to hoping we stick with each other whatever happens.\" We had a fight but we laughed so much after.",
		spotify: {
			artist: "Brenda Lee",
			album: "Emotions",
			song: "If You Love Me (Really Love Me)",
			link: "https://open.spotify.com/track/1U9n0AGpu9X69SeaMJfs8R?si=9d8f418016c942aa"
		}
	}, {
		status: "good",
		reason: "We had a fun call today. I openly joked about the hard topics discussed from yesterday. She noted that, even though our pains are still fresh, it's also great that we can laugh about it.",
		spotify: {
			artist: "TWICE",
			album: "MOONLIGHT SUNRISE",
			song: "MOONLIGHT SUNRISE",
			link: "https://open.spotify.com/track/5NcLyVjUgG0yfwHgr5t81w?si=3cc4d4b2dd844877"
		}
	}, {
		status: "excellent",
		icon: "air_freshener",
		title: "Our First Hangout Together",
		reason: "It was the first time since we actually went out and did stuff together. Even though we waited more than an hour for food, we had fun eating. We got to make perfume after and had a lot of fun just talking in the car rides.",
		spotify: {
			artist: "Passion Pit",
			album: "Kindred",
			song: "Whole Life Story",
			link: "https://open.spotify.com/track/3ZdtwUI2bPlq409wroQRmK?si=f60a686b9f8a4e5e"
		}
	}, {
		status: "awful",
		icon: "sentiment_sad",
		title: "Goodbye, My Besties",
		reason: "Said a final goodbye to ex-best friends this day. Oh well. It was a waste of time, but whatever. Neither of them really treasured our memories as much as I did and we all ended on horrible terms. One of them was really hostile towards me and acted like such a hypocrite. I can't believe I spent that much time on them for nothing.",
		spotify: {
			artist: "Kitty Kallen",
			album: "Little Things Mean a Lot",
			song: "It's Been a Long, Long Time",
			link: "https://open.spotify.com/track/2jBk4wSHTYON7kgHZ7Ob5J?si=318767bcf6c44538"
		}
	}, {
		status: "average"
	}, {
		status: "bad",
		reason: "Had a fight, tried to watch Harry Potter together, but we couldn't finish it."
	}, {
		status: "above-average",
		title: "Camera Hijinks",
		reason: "I can't believe I'll get her the camera today."
	}, {
		status: "above-average",
		reason: "I had some misaligned expectations on my end. So we came to a compromise. I got what I wanted and I couldn't be happier."
	}, {
		status: "good",
		reason: "We spent most of the day together again! After discussing our situation from yesterday, we came to a mutual agreement that we both think is better for us. And we ate dinner together today! We also watched a lot of relay dances from ITZY, Twice, Aespa."
	}, {
		status: "above-average",
		icon: "auto_fix",
		reason: "We played Hogwarts Legacy today! I also met up with a friend on Slowly that I've been talking to since 2021!",
		spotify: {
			artist: "BLACKPINK",
			album: "BORN PINK",
			song: "Tally",
			link: "https://open.spotify.com/track/6Ho58vQUjGnBU9m8Z6uNcv?si=7515421dc54e4f3a"
		}
	}, {
		status: "excellent",
		icon: "camera_outdoor",
		title: "Outside, Open Communication",
		reason: "We went out again today! It was a really hard day to go through. We had some arguments and some issues, but we had fun outside. We took lots of photos outside, too. This was a real test of our communication skills.",
		spotify: {
			artist: "IZ*ONE",
			song: "La Vie en Rose",
			link: "https://open.spotify.com/track/3WfaJhCL4p2JbdffJjV6Va?si=b2ac2f5bd0be4997",
			album: "COLOR*IZ"
		}
	}, {
		status: "average",
		title: "Town Hall's Last Day Together",
		reason: "I'm still shaken by what happened yesterday, so I just tried to sleep it off. Our friend group had one last call today and things ended after today."
	}, {
		status: "excellent",
		icon: "volunteer_activism",
		title: "Valentine's Day!",
		reason: "It was Valentine's Day! As usual, we spent the day together. I gave her chocolates in exchange for that thing that she's been putting off. Even though it was an exchange for something, I still gave her chocolates."
	}, {
		status: "above-average",
		reason: "Another enjoyable day with her today. I kept her company as she was going through her day.",
		spotify: {
			artist: "Megan Thee Stallion, Dua Lipa",
			song: "The Sweetest Pie",
			link: "https://open.spotify.com/track/7mFj0LlWtEJaEigguaWqYh?si=e714de189c8f4921",
			album: "Sweetest Pie"
		}
	}, {
		status: "above-average",
		reason: "I watched Quantumania in theaters today!"
	}, {
		status: "average",
		spotify: {
			artist: "The Kid LAROI, Justin Bieber",
			album: "STAY (with Justin Bieber)",
			song: "STAY (with Justin Bieber)",
			link: "https://open.spotify.com/track/5HCyWlXZPP0y6Gqq8TgA20?si=0cc4de07d5fb4b8b"
		}
	}, {
		status: "excellent",
		icon: "forest",
		title: "Elaine and the Magic Forest",
		reason: "I bought a Nintendo Switch today! Also, I met up with my big sister Elaine after YEARS. We took photos at this cool place in Quezon City. It had several cool rooms and a live performance at the end."
	}, {
		status: "average"
	}, {
		status: "good"
	}, {
		status: "good",
		reason: "We spent the entire day together and we played It Takes Two! We both had a fun day playing it together.",
		spotify: {
			artist: "Big Time Rush",
			album: "Paralyzed",
			song: "Paralyzed",
			link: "https://open.spotify.com/track/6KOQLj5dcYuFSN6srNs96u?si=7d4d6ac985214ee2"
		}
	}, {
		status: "good",
		reason: "We spent the ENTIRE DAY together. Jeez! It was one long day with her and we just played Sims in the evening. This whole day was such a blast, I swear."
	}, {
		status: "above-average",
		reason: "We spent the majority of the day together again. She was gone for some time, idk maybe an hour and 30 minutes, but she came back and we called, played Sims for 2 hours more. It was a long day again but I had fun because she was by my side throughout all of it."
	}, {
		status: "awful",
		icon: "person_search",
		title: "Case Closed",
		reason: "I am sick of this. I'm just done. I'm leaving it at that."
	}, {
		status: "bad",
		reason: "Being alone feels bittersweet. On one side, I had a lot of weight shed off of my shoulders. But on one hand, I miss them so much and it's honestly kind of lonely without them.",
		spotify: {
			artist: "Jay & The Americans",
			album: "Come A Little Bit Closer",
			song: "Come A Little Bit Closer",
			link: "https://open.spotify.com/track/0cLZ9ecuhocv99BICMb59O?si=a5f882dd01d94c27"
		}
	}, {
		status: "below-average",
		reason: "Being alone hits deep. Dang."
	}, {
		status: "awful",
		icon: "local_fire_department",
		reason: "We haven't talked since Friday. My friends were there for me, and so did the other people who knew my situation, so I'm thankful for them. Still, it hurts, it hurts, it feels like a blade was directly running through my heart."
	}, {
		status: "below-average",
		reason: "We lightly talked and parted ways after having been together. It was lonely getting home. The day was so nice, the sun was out, and I would have loved to spend the rest of the day talking to my special person about it. It just sucks. I'm glad I had friends with me that made it a 1,000 times better.",
		spotify: {
			artist: "NewJeans",
			song: "OMG",
			link: "https://open.spotify.com/track/65FftemJ1DbbZ45DUfHJXE?si=078c8b7e09214fdf",
			album: "NewJeans 'OMG'"
		}
	}, 
];

export default februaryMap;
