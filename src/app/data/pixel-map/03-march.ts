import IPixelMapDay from "@interfaces/pixel-map-day.interface";

const marchMap: Array<IPixelMapDay> = [
    {
		status: "bad",
		reason: "It was another day spending it alone. Even though we lightly talked in the evening, we parted ways shortly thereafter.",
	},
    {
		status: "below-average",
		reason: "We got to talking again, this time on an app. It was weird. Our situation was still not cleared up but we were still friends, in a way. I don't know what to do or how this will work out. She's still here, surprisingly.",
	},
    {
		status: "good",
		icon: "handshake",
		title: "We (Ee!) Are Getting Back Together",
		reason: "We got back to talking on Discord. We called in the morning and it felt familiar for the both of us, so we got space. Until the evening, we both called each other again. Even though it was a bit suspicious that she joined after doing something strange, I shouldn't care anymore. We aren't in that spot.",
		spotify: {
			artist: "Taylor Swift",
			album: "Red (Taylor's Version)",
			song: "We Are Never Ever Getting Back Together (Taylor's Version)",
			link: "https://open.spotify.com/track/5YqltLsjdqFtvqE7Nrysvs?si=e38c83895195412b"
		}
	},
    {
		status: "good",
		reason: "That was a fun video call ngl.",
	},
	{
		status: "above-average",
		reason: "I felt like I got a lot of stuff done today. I cleaned my room and did a lot of stuff."
	},
	{
		status: "good",
		reason: "We watched Friends today and it was our first workday together again. It was fun, spending all day with her is always appreciated. I got cool things as well so that really completed my day."
	},
	{
		status: "below-average",
		reason: "Something felt lacking, and off. Kinda weird."
	},
	{
		status: "above-average",
		reason: "Someone prepared a budget plan for me today! It's cute that they put in the effort to help me out with my finances. We played some mini games in the evening and it was overall a pretty fun day. And I finally fixed the thing I've been stuck with at work for the past month!"
	},
	{
		status: "awful",
		icon: "delete",
		title: "Collapse",
		reason: "I knew it, to be honest."
	},
	{
		status: "special",
		icon: "attractions",
		title: "Theme Park Shenanigans",
		reason: "Overall the mood of the morning was pretty horrible. I didn't get any good sleep, I wasn't able to rest at all. Then I saw this video on Instagram of this rollercoaster. That's when I had the idea: I should go to a theme park! So I did. I tried my best to cheer myself up and I was just by myself. It took some time, but I learned how to build myself up that day. And it showed that I still appreciate my own company and I can validate my own experiences.",
		spotify: {
			artist: "Taylor Swift",
			album: "Speak Now (Deluxe Package)",
			song: "The Story Of Us",
			link: "https://open.spotify.com/track/7vBlnGdfOLzVEqiOQQxeU8?si=4fc8ab56d86948fa"
		}
	},
	{
		status: "excellent",
		icon: "king_bed",
		title: "RC Boys' 3rd Sleepover",
		reason: "I had so much fun with the RC boys today. We ate a lot of food, drank a lot. I tagged along with JK to Alabang for most of the day and the scenery was pretty relaxing. I tried to forget about her yet a voice was scratching at the back of my head. The boys tried to guide me on other stuff, though, while watching Magic Mike."
	},
	{
		status: "awful",
		icon: "personal_injury",
		title: "The Worst Day of 2023 (So Far)",
		reason: "What a horrible day, I swear. I lost my Samsung Galaxy Buds and I had to dumpster dive just to find them. Fuck my life, they're gone now. What a waste. I left them specifically on the table and now they're gone. Not only that, but I got scammed! I lost so much money. And to top it all off, I got sprained when I went outside because of a random bout of overthinking. Jesus. I can't believe this day, this was just horrible."
	},
	{
		status: "above-average",
		reason: "We got to be in a video call for the first time in a while. But overall, we had a good day together."
	},
	{
		status: "good",
		reason: "We spent the majority of the day together again. Wow, it feels like a long time since we've done this tbh."
	},
	{
		status: "above-average",
		icon: "diamond",
		reason: "We played Sims 4 today!"
	},
	{
		status: "bad",
		reason: "I had a fight that wasn't able to fix. Wth did I do this time? Why does it always happen on a Thursday or at the end of the weekdays? I also watched The Menu today."
	},
	{
		status: "below-average",
		icon: "mic",
		reason: "We didn't talk after that but my guy best friend gave me a heartfelt letter! And I got song covers! We spent the entire night calling and even Max joined. It was a pretty fun call, the 5 hours just flew by, to be honest."
	},
	{
		status: "above-average",
		reason: "Had a fun call with Kai and we stalked her Instagram profile, and we made fun of it. They use Venice in their 3D renderings haha."
	},
	{
		status: "good",
		icon: "playlist_play",
		reason: "I watched Don't F**K With Cats with a friend and then I had some closure, in a way. And a friend named Kai prepared a playlist for me to listen to. It has like 1,000 songs 😵‍💫"
	},
	{
		status: "above-average",
		reason: "I had set new boundaries today. Here's to hoping that these boundaries help me get more work done."
	},
	{
		status: "good",
		reason: "I watched Fear Street with a friend today and called other my bros afterwards."
	},
	{
		status: "excellent",
		reason: "Playing Left 4 Dead was fun!"
	},
	{
		status: "good",
		reason: "I watched American Psycho and drank alcohol. I had a burning feeling and I almost would've passed out because I didn't eat anything, but someone suggested that I eat something and I felt okay afterwards."
	},
	{
		status: "special",
		icon: "explosion",
		title: "Everything Didn't Blow Up?",
		reason: "A big fight could've ensued today, to be honest. But we talked it out. Today could've been another make-or-break day and we didn't break. I'm honestly surprised. And I got to know more about a friend today! We got to play 2 truths and 1 lie, and it was pretty hard to play.",
		spotify: {
			artist: "KC & The Sunshine Band",
			album: "All In a Night's Work (Expanded Version)",
			song: "Give It Up",
			link: "https://open.spotify.com/track/3yDhZq8f17SmumVmEyCaRN?si=5ad81db87bfa4d1d"
		}
	},
	{
		status: "above-average",
		reason: "I couldn't meet up with friends today but I generally had a chill day at home. And I had fun in the evening. A friend sent me beach photos and promised to give me blueberry jam, so that was nice."
	},
	{
		status: "average"
	},
	{
		status: "good",
		title: "Shot at Redemption",
		reason: "Some boss commended me and I was told that I've been bouncing back well ever since January. I guess I'm not as stressed as I thought I was. "
	},
	{
		status: "above-average",
		reason: "I watched someone stream Oregon Trail on their iPad and we had fun."
	},
	{
		status: "above-average",
		reason: "I'm just tired today, but I enjoyed the bulk of the day."
	},
	{
		status: "above-average",
		reason: "We had a hard discussion over Tetris. I thought this would be the start of a large fight or argument, but we proved to be okay. I got to eat Krispy Kreme and it was generally an okay day."
	},
	{
		status: "above-average",
		reason: "I had fun watching Tetris. Today was fun, regardless.",
		spotify: {
			song: "Cupid - Twin Ver.",
			artist: "FIFTY FIFTY",
			link: "https://open.spotify.com/track/3Kw7zkALCVxY4wmlnh2IWC?si=56f9c55cc7aa4005",
			album: "The Beginning: Cupid"
		}
	}
];

export default marchMap;
