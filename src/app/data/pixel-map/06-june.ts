import IPixelMapDay from "@interfaces/pixel-map-day.interface";

const juneMap: Array<IPixelMapDay> = [
	{
		status: "above-average",
		title: "Across the Spider-Verse",
		icon: "cinematic_blur",
		reason: "I watched Spider-Man: Across the Spider-Verse today and it was pretty inspiring.",
		spotify: {
			artist: "LE SSERAFIM",
			album: "Unforgiven",
			song: "Flash Forward",
			link: "https://open.spotify.com/track/74cpuIw43kA8xPgbQEPdss?si=6c795047e36749c9"
		}
	},
	{
		status: "above-average",
		reason: "I spent some parts of the day with someone in a call again, just sticking by them. I finally decided to gift them some macarons and sweets for our 500th day anniversary thing. In the afternoon, I also bought and packaged some sweets and candy for Kai, then I included my old phone. It's been a while since I've been this giving and I feel a little better by giving.",
		spotify: {
			artist: "STAYC",
			song: "LOVE FOOL",
			link: "https://open.spotify.com/track/2OhOkew9BkkLTGBG9cFOz5?si=d7498fc4888f48c7",
			album: "STAYDOM"
		}
	},
	{
		status: "above-average",
		reason: "I mostly did chores today, I finally cleaned all of the plates and utensils into that new dishwasher set.",
		spotify: {
			artist: "STAYC",
			song: "SO WHAT",
			link: "https://open.spotify.com/track/6hC2Qn11Fzw4Ufi4XH6z2m?si=4dc3c4bdfbe04889",
			album: "STAYDOM"
		}
	},
	{
		status: "awful",
		title: "The Pressure Cooker",
		icon: "cooking",
		reason: "This was a hard day to get through. I felt the full weight of my future on my shoulders. I met up with a friend, Nathan, to discuss things with and he offered great support. I haven't seen the guy in years and I think he's doing great. I appreciate him being there for me, even bringing along his gf."
	},
	{
		status: "above-average",
		title: "A Litany Against Fear",
		reason: "I watched someone play Hogwarts Legacy and then I played Phasmophobia with them after work. I also read some parts of Dune and Pride & Prejudice.",
	},
	{
		status: "above-average",
		reason: "It was an okay day, I felt overly-busy at work and I passed out after playing Phasmo.",
		spotify: {
			artist: "Vengaboys",
			song: "Shalala Lala",
			link: "https://open.spotify.com/track/4pV49P2v3sxI0XYjUTmOl1?si=3a9a2f1ea2e74a5c",
			album: "The Platinum Album"
		}
	},
	{
		status: "above-average",
		reason: "I'm trying to get better at playing Phasmo. I talked to other people as well today, including Kai. My time alone after work was fun.",
		spotify: {
			artist: "LE SSERAFIM",
			album: "Unforgiven",
			song: "UNFORGIVEN",
			link: "https://open.spotify.com/track/51vRumtqbkNW9wrKfESwfu?si=91a7ed9f902f40d6"
		}
	},
	{
		status: "above-average",
		spotify: {
			artist: "PinkPanthress, Ice Spice",
			album: "Boy's a liar Pt. 2",
			song: "Boy's a liar Pt. 2",
			link: "https://open.spotify.com/track/6AQbmUe0Qwf5PZnt4HmTXv?si=e2b345c1591b4f58"
		}
	},
	{
		status: "above-average",
		reason: "I have made the decision to fight for my life today. I really should do better, and I'm gonna.",
		spotify: {
			artist: "SEVENTEEN",
			album: "SEVENTEEN 10th Mini Album 'FML'",
			song: "F*ck My Life",
			link: "https://open.spotify.com/track/3tgWMPOY4stCdKYj5NjrAe?si=4d3cc388b1404286"
		}
	},
	{
		status: "good",
		icon: "flashlight_on",
		title: "Who You Gonna Call?",
		reason: "I had fun playing Phasmophobia with someone and their cousin. I also started work (and completed) that *actual* dynamic theming for my website. I also ate good food today, which is always nice.",
		spotify: {
			artist: "Taylor Swift",
			album: "Lover",
			song: "London Boy",
			link: "https://open.spotify.com/track/1LLXZFeAHK9R4xUramtUKw?si=982fab4c655b441a"
		}
	},
];

export default juneMap;
