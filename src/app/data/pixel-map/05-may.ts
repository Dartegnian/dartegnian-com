import IPixelMapDay from "@interfaces/pixel-map-day.interface";

const mayMap: Array<IPixelMapDay> = [
	{
		status: "below-average",
        reason: "I had this weird feeling that things are going to end someday.",
		spotify: {
			artist: "Meghan Trainor, John Legend",
			album: "Title (Deluxe)",
			song: "Like I'm Gonna Lose You",
			link: "https://open.spotify.com/track/2YlZnw2ikdb837oKMKjBkW?si=00a266cafae8408b"
		}
	},
	{
		status: "bad",
        reason: "The only thing that really made this day okay was that Le Sserafim released a new album and a lot of MVs.",
		spotify: {
			artist: "LE SSERAFIM",
			album: "Unforgiven",
			song: "Flash Forward",
			link: "https://open.spotify.com/track/74cpuIw43kA8xPgbQEPdss?si=6c795047e36749c9"
		}
	},
	{
		status: "above-average",
        reason: "I got my passport and a lot of stuff done today."
	},
	{
		status: "above-average",
		reason: "I got through a severe time crunch today and I think I handled it well, honestly. The songs I played today definitely helped.",
		spotify: {
			artist: "LE SSERAFIM",
			album: "Unforgiven",
			song: "Fire in the belly",
			link: "https://open.spotify.com/track/05RlBHEZg1RmL9DnPgv9Qq?si=5f58bb7ea87f4f1c"
		}
	},
	{
		status: "above-average",
		reason: "I was able to breathe a sigh of relief today as all my major tasks were accomplished, and I watched Becoming Jane. And I got to play Stardew Valley again!",
		spotify: {
			artist: "Men At Work",
			album: "Business As Usual",
			song: "Who Can It Be now?",
			link: "https://open.spotify.com/track/5rfJ2Bq2PEL8yBjZLzouEu?si=4e0203f423364a6d"
		}
	},
	{
		status: "awful",
		title: "Fake Friends until the End",
		icon: "person_off",
		reason: "I've been thinking of my former friends for this week. How they come and go, especially my IMC batchmate friends and Angela. I've tried all that I can for them. For the IMC friends, I've showed them that I don't run to them with my problems anymore. And for Angela, I've tried to communicate with her and even apologized directly. Both are lost causes. There were no compromises on their end. At this point, it's better to just delete them from my memory and cut them off.",
		spotify: {
			artist: "Big Time Rush",
			album: "Paralyzed",
			song: "Paralyzed",
			link: "https://open.spotify.com/track/6KOQLj5dcYuFSN6srNs96u?si=7d4d6ac985214ee2"
		}
	},
	{
		status: "excellent",
		title: "Friends 'Til the End",
		icon: "diversity_3",
		reason: "I actually got to hang out with a friend group that wanted me to be with them. A friend group that, despite my turbulence with them, has been solid for me. They still welcomed me and we had fun bowling. It's nice to see them have fun with their SOs.",
		spotify: {
			artist: "Big Time Rush",
			album: "Paralyzed",
			song: "Paralyzed",
			link: "https://open.spotify.com/track/6KOQLj5dcYuFSN6srNs96u?si=7d4d6ac985214ee2"
		}
	},
	{
		status: "above-average",
		reason: "Aespa released a new album today! I was excited for it and it lived up to the hype, I got to watch it with someone.",
		spotify: {
			artist: "aespa",
			album: "MY WORLD - The 3rd Mini Album",
			song: "Spicy",
			link: "https://open.spotify.com/track/1ULdASrNy5rurl1TZfFaMP?si=d25d8922b1bd4b9c"
		}
	},
	{
		status: "below-average",
		reason: "One of my new profiles got banned, but thankfully I was able to confide in a few people about what I was actually feeling.",
		spotify: {
			artist: "Hoàng Thùy Linh",
			album: "See Tình (Speed Up Version)",
			song: "See Tình (Speed Up Version)",
			link: "https://open.spotify.com/track/5sCATdnR2mWbHgdmIXBFHa?si=9d6f9fd1fbdd44d8"
		}
	},
	{
		status: "above-average",
		reason: "I worked on the farm in Stardew a lot today, it was pretty fun.",
		spotify: {
			artist: "aespa",
			album: "MY WORLD - The 3rd Mini Album",
			song: "Salty & Sweet",
			link: "https://open.spotify.com/track/4wQDjZtXjsFtU3BLSiIH4t?si=031e97c5b49d4ae5"
		}
	},
	{
		status: "above-average",
		reason: "I felt so anxious today for a lot of things. I felt overwhelmed by the amount of things that went on today. I made it up in the evening by focusing on improving my website's performance and pranking friends on IG. I got to have fun today, and that's what mattered."
	},
	{
		status: "average",
		reason: "I had a lot of free time in the evening today and I played a fun game of Scrabble with someone. I lost Scrabble.",
		spotify: {
			artist: "LE SSERAFIM",
			album: "Unforgiven",
			song: "Flash Forward",
			link: "https://open.spotify.com/track/74cpuIw43kA8xPgbQEPdss?si=6c795047e36749c9"
		}
	},
	{
		status: "bad",
		icon: "pool",
		title: "Staying Afloat",
		reason: "I did a lot of stuff this week! I had to work as well today, on a Saturday. I worked from  9 a.m. in the morning until 9 p.m. in the evening. I finished a lot of work today and I played Stardew after.",
		spotify: {
			artist: "Apink",
			album: "UNE ANNEE",
			song: "BUBIBU",
			link: "https://open.spotify.com/track/08Az8oHUOLj2TjMSYtWCd5?si=090a2f331b8b4d78"
		}
	},
	{
		status: "below-average",
		reason: "This was my one and only weekend for the week and I didn't feel rested enough. I ate good food and played a lot of PC games. But I just didn't feel well-rested enough.",
		spotify: {
			artist: "Rosa Walton, Hallie Coggins",
			album: "Cyberpunk 2077: Radio, Vol. 2 (Original Soundtrack)",
			song: "I Really Want to Stay at Your House",
			link: "https://open.spotify.com/track/7mykoq6R3BArsSpNDjFQTm?si=55733def470d4a06"
		}
	},
	{
		status: "above-average",
		reason: "I spent most of the day playing a lot of Stardew Valley!",
	},
	{
		status: "below-average",
		reason: "I felt very disconnected from everything today, idk why. I got a lot of stuff done but things just felt empty and far away.",
	},
	{
		status: "bad",
		reason: "Something bad happened. I really was too tired to deal with things but I tried my best to, even though I felt unfeeling about everything. I somehow fixed everything, but I'm surprised.",
		spotify: {
			artist: "Kero Kero Bonito",
			album: "Only Acting",
			song: "Only Acting",
			link: "https://open.spotify.com/track/30uMAXUfeQHEXiTUC76hLI?si=1074b3b60299403b"
		}
	},
	{
		status: "above-average",
		reason: "My Aespa album finally arrived! And I got lucky with the photocards I got!",
		spotify: {
			artist: "aespa",
			album: "MY WORLD - The 3rd Mini Album",
			song: "Spicy",
			link: "https://open.spotify.com/track/1ULdASrNy5rurl1TZfFaMP?si=d25d8922b1bd4b9c"
		}
	},
	{
		status: "above-average",
		reason: "I stood up late with someone today. We still had a good day, regardless.",
		spotify: {
			artist: "The J. Geils Band",
			album: "Freeze Frame",
			song: "Centerfold",
			link: "https://open.spotify.com/track/1ynmMEK1fkyiZ6Z6F3ThEt?si=ef1c3b7d64284c49"
		}
	},
	{
		status: "above-average",
		title: "Super Idol's Smile",
		icon: "history",
		reason: "I mostly spent the day by myself today, going back on old photos with friends, playing that game we used to play. I tried to go back to a time period where things were okay. I miss sharing Super Idol memes with my friends. In a way, I kinda miss 2021. In the evening Kai convinced me to eat outside and try out her favorite ice cream, I also ate samgyup and had fun playing music in BGC.",
		spotify: {
			artist: "A Si",
			album: "热爱105°C的你",
			song: "热爱105°C的你",
			link: "https://open.spotify.com/track/5RKWxItZvYY9wtKJuy2Hb7?si=86ce8ed4044f4f97"
		}
	},
	{
		status: "special",
		title: "When Things Fell Apart",
		icon: "broken_image",
		reason: "I've been thinking about it and I've decided to commit to it. I didn't know what to do, but I know I'll be okay.",
		spotify: {
			artist: "Beabadobee",
			album: "Coffee",
			song: "Coffee",
			link: "https://open.spotify.com/track/429NtPmr12aypzFH3FkN9l?si=e8fa20637ac249fc"
		}
	},
	{
		status: "above-average",
		reason: "I had fun playing Stardew with someone today."
	},
	{
		status: "above-average",
		spotify: {
			artist: "A Si",
			album: "热爱105°C的你",
			song: "热爱105°C的你",
			link: "https://open.spotify.com/track/5RKWxItZvYY9wtKJuy2Hb7?si=86ce8ed4044f4f97"
		}
	},
	{
		status: "excellent",
		title: "Ready, Story, Go",
		reason: "I got that Story Viewer from Google to work on my website! I'm so excited for all the things I can do with it and I just played Danza Kuduro all night",
		spotify: {
			artist: "Don Omar",
			album: "Meet The Orphans",
			song: "Danza Kuduro",
			link: "https://open.spotify.com/track/2a1o6ZejUi8U3wzzOtCOYw?si=10a41ea600a44175"
		}
	},
	{
		status: "below-average",
		reason: "I didn't have a lot of sleep today, but the evening turned out okay as I was able to sleep through for the most of it.",
		spotify: {
			artist: "Doja Cat",
			album: "Planet Her",
			song: "Woman",
			link: "https://open.spotify.com/track/6Uj1ctrBOjOas8xZXGqKk4?si=fee95bef7aba4b3f"
		}
	},
	{
		status: "above-average",
		reason: "I stood up late with someone and watching The Office with them. I also fixed a problem and issued a pull request to an NPM package that I use for this website. I also fixed and did a lot of tasks at work. So, needless to say, I feel very accomplished today.",
		spotify: {
			artist: "fun\.",
			album: "Aim and Ignite",
			song: "At Least I'm Not as Sad (as I Used to Be)",
			link: "https://open.spotify.com/track/11WtwFRGLTLFp0FEZi2fnE?si=486e3aa5b3ee4439"
		}
	},
	{
		status: "excellent",
		icon: "merge",
		title: "Pull & Merge",
		reason: "I spent most of my day outside and had a really fun time working on the Stories feature of my website. I tried so hard to push it into production and got it to work! Plus, my pull request for an NPM package I use got approved and deployed.",
		spotify: {
			artist: "Tomoko Aran",
			album: "浮遊空間",
			song: "Midnight Pretenders",
			link: "https://open.spotify.com/track/0JUWF44gfMszGNhjCF7Ufs?si=3204f86c4e4f4c97"
		}
	},
	{
		status: "above-average",
		reason: "I didn't know what to do today. I finally launched the Stories feature of my website and fixed a lot of things.",
		spotify: {
			artist: "Y-Not",
			album: "Mamma's Boy",
			song: "Average Joe",
			link: "https://open.spotify.com/track/35QpMgp1pheSPJegwzdg5e?si=9b0ec5fc96344901"
		}
	},
	{
		status: "below-average",
		icon: "sync_problem",
		title: "In the Middle of the Night",
		reason: "Somehow I started to feel conflicted in the evening and I kept singing Untouchable out of the blue. When I read the lyrics, it made sense why I started to like it.",
		spotify: {
			artist: "Taylor Swift ",
			album: "Fearless (Taylor's Version)",
			song: "Untouchable",
			link: "https://open.spotify.com/track/0tQ9vBYpldCuikPsbgOVKA?si=6701731ea1da4580"
		}
	}, {
		status: "below-average",
		reason: "I didn't feel okay today, honestly.",
		spotify: {
			artist: "Bruno Major",
			song: "Regent's Park",
			link: "https://open.spotify.com/track/2qiDXgq5fL1FetkhX5xi9L?si=66ba7e955b4f4b82",
			album: "To Let A Good Thing Die"
		}
	}, {
		status: "above-average",
		title: "That's New?",
		reason: "It seemed like a normal day today, the status quo returned and everything seemed normal. But I felt something weird when a person was telling me about their day. This was probably nothing, right? It is? That was weird. But someone shared some new songs with me and I got to earn more than a million in Stardew Valley today.",
		spotify: {
			artist: "b/t",
			song: "Blossom",
			link: "https://open.spotify.com/track/0zHm0VIyI9N86mfNXKcoIw?si=768fbdae0576443c",
			album: "Blossom"
		}
	},
];

export default mayMap;
