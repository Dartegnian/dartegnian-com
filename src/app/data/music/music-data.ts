const featuredArtist = {
	artist: "aespa",
	spotify: "https://open.spotify.com/artist/6YVMFz59CuY7ngCxTxjpxE?si=OZYRYVcBRyGFuMTGUcotcQ",
	description: `Aespa is a South Korean girl group formed by SM Entertainment. The group consists of four members: Karina, Giselle, Winter, and Ningning. They debuted on November 17, 2020, with the single "Black Mamba". The group's name, Aespa, combines the English initials of "avatar" and "experience" (Avatar X Experience) with the English word "aspect", meaning "two sides", to symbolize the idea of "meeting another self and experiencing the new world".`,
	image: "",
	albums: [
		{
			title: "MY WORLD",
			link: "https://open.spotify.com/album/69xF8jTd0c4Zoo7DT3Rwrn?si=boN-nuxsRQmk60m-n_4bpg",
			image: ""
		},
		{
			title: "Girls",
			link: "https://open.spotify.com/album/4w1dbvUy1crv0knXQvcSeY?si=Rm58_C1sRZaEI1OxlJfqUA",
			image: ""
		},
		{
			title: "Hold On Tight",
			link: "https://open.spotify.com/album/4bWGRs1SqNwFXaRDXRAANN?si=gtYrqK7KTGC0NEItOLGuSg",
			image: ""
		},
		{
			title: "Savage",
			link: "https://open.spotify.com/album/3vyyDkvYWC36DwgZCYd3Wu?si=F8IyH_V0Tn67aJQzKfnN_Q",
			image: ""
		},
		{
			title: "Life's Too Short",
			link: "https://open.spotify.com/album/11lLYKMkFheiV7ObD7WCnx?si=vHtYMuZHRkCLG8N88pWaGA",
			image: ""
		},
	]
};

const musicData = [
	// {
	// 	image: "toptrackmonth",
	// 	icon: "volume_up",
	// 	heading: "OMG by NewJeans",
	// 	subHeading: "Top track this " + new Date().toLocaleString('default', { month: 'long' }),
	// 	linkText: "Watch on YouTube",
	// 	link: "https://www.youtube.com/watch?v=sVTy_wmn5SU"
	// },

	// {
	// 	image: "toptrackyear",
	// 	icon: "repeat_one",
	// 	heading: "OMG by NewJeans",
	// 	subHeading: "Song of the year",
	// 	linkText: "Listen on Spotify",
	// 	link: "https://open.spotify.com/track/65FftemJ1DbbZ45DUfHJXE?si=4553e35ec77e47ba"
	// },

	{
		image: "favegenre",
		icon: "graphic_eq",
		heading: "Vocaloid",
		subHeading: "Favorite genre at the moment",
		linkText: "Find on Spotify",
		link: "https://open.spotify.com/playlist/37i9dQZF1EIdUz13xg2Opj?si=fd704d29dbc147e4"
	},

	// {
	// 	image: "girlsgeneration",
	// 	icon: "headphones",
	// 	heading: "Girls' Generation",
	// 	subHeading: "Favorite girl group",
	// 	linkText: "Listen on Spotify",
	// 	link: "https://open.spotify.com/artist/0Sadg1vgvaPqGTOjxu0N6c"
	// },

	{
		image: "favealbum",
		icon: "library_music",
		heading: "Girls & Peace by Girls' Generation",
		subHeading: "Favorite album",
		linkText: "Listen on YouTube",
		link: "https://www.youtube.com/watch?v=U-BNEOThAIY&list=PLMpIDAYcfx9zMWWKb78uIaxd2OOPl21S4"
	},

	// {
	// 	image: "pexels-john-tekeridis-340103",
	// 	icon: "radio",
	// 	heading: "Spotify",
	// 	subHeading: "Streaming platform",
	// 	linkText: "Check website",
	// 	link: "https://open.spotify.com/"
	// },

	{
		image: "favevocaloidalbum",
		icon: "library_add",
		heading: "IA/02 -COLOR-",
		subHeading: "Favorite Vocaloid album",
		linkText: "Listen on Spotify",
		link: "https://open.spotify.com/album/1RTOpEWZgUlLHhZakgQsQp"
	},

	{
		image: "owlcity",
		icon: "spatial_audio_off",
		heading: "Owl City",
		subHeading: "Favorite solo artist",
		linkText: "Listen on Spotify",
		link: "https://open.spotify.com/artist/07QEuhtrNmmZ0zEcqE9SF6"
	},

	{
		image: "faveband",
		icon: "speaker_group",
		heading: "Paramore",
		subHeading: "Favorite band",
		linkText: "Listen on Spotify",
		link: "https://open.spotify.com/artist/74XFHRwlV6OrjEM0A2NCMF"
	},

	{
		image: "luka",
		icon: "discover_tune",
		heading: "Luka Megurine",
		subHeading: "Favorite Vocaloid",
		linkText: "Listen on YouTube",
		link: "https://www.youtube.com/results?search_query=luka+megurine"
	},

	// {
	// 	image: "pexels-jerson-vargas-4765390",
	// 	icon: "speed",
	// 	heading: "130+",
	// 	subHeading: "Preferred BPM",
	// 	linkText: "My upbeat playlist",
	// 	link: "https://open.spotify.com/playlist/24hWaH3lri5CVXvTuzqqHX?si=1576583ba5ef4c0b"
	// },
];

export { featuredArtist, musicData };
