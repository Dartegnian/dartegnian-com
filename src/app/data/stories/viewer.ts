const storyCards = [
	{
		headline: "My Music Tastes",
		link: "/stories/music",
		coverImg: "music-likes",
		coverImgAlt: "Albums",
		icon: "music_note",
		topic: "Music"
	},
	{
		headline: "Close Friends, Companions, Besties",
		link: "/stories/friends",
		coverImg: "friends",
		coverImgAlt: "One of my closest friends",
		icon: "diversity_3",
		topic: "Friends"
	},
	{
		headline: "2023 Through My Lens",
		link: "/stories/current-year",
		coverImg: "current-year",
		coverImgAlt: "One of my favorite photos of 2023",
		icon: "calendar_month",
		topic: "2023"
	},
	{
		headline: "Pet Showcase: My Beloved Cat",
		link: "/stories/cat",
		coverImg: "cat-story",
		coverImgAlt: "My beloved cat, Milktea",
		icon: "pets",
		topic: "Pets"
	},
	{
		headline: "The Best Places I've Been To",
		link: "/stories/places",
		coverImg: "places-story",
		coverImgAlt: "A photo of the sea in Subic, Zambales",
		icon: "pin_drop",
		topic: "Places"
	},
	{
		headline: "Core Memories As Photos",
		link: "/stories/core-memories",
		coverImg: "core-memories",
		coverImgAlt: "Filling Station Bar Cafe in Poblacion, Makati",
		icon: "psychology",
		topic: "Memories"
	},
	{
		headline: "Best Pics, Snaps, Clicks",
		link: "/stories/best-photos",
		coverImg: "best-photos",
		coverImgAlt: "The MOA Eye ferris wheel at sunset",
		icon: "photo_camera",
		topic: "Photos"
	},
];

export default storyCards;
