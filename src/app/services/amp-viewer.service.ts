import { Injectable } from '@angular/core';
import { easeOutQuad, lerp } from "@utils/amp-viewer/math";

@Injectable({
	providedIn: 'root'
})
export class AmpViewerService {
	cards: any;
	cardMargin: any;
	cardWidth: any;

	player: any;
	doc: any;
	lightboxEl: any;

	scaleVal = 1;
	scalingDown = false;
	toggleThresholdPx = 60;
	deltaY = 0;
	isSwipeY: boolean | null = null;
	touchStartX = 0;
	touchStartY = 0;

	constructor() { }

	setDocument(doc: any) {
		this.doc = doc;
		this.setPlayer(this.doc.querySelector('amp-story-player'));
	}

	setPlayer(playerEl: any) {
		this.player = playerEl;
	}

	setLightbox(lightbox: any) {
		this.lightboxEl = lightbox;
	}

	setCards(cardEls: any) {
		this.cards = cardEls;
	}

	setCardWidth(width: any) {
		this.cardWidth = width;
	}

	setCardMargin(margin: any) {
		this.cardMargin = margin;
	}

	/**
	 * Initializes listeners.
	 */
	initializeTouchListeners() {
		this.player.addEventListener('amp-story-player-touchstart', (event: any) => {
			this.onTouchStart(event);
		}, {passive: true});

		this.player.addEventListener('amp-story-player-touchmove', (event: any) => {
			this.onTouchMove(event);
		}, {passive: true});

		this.player.addEventListener('amp-story-player-touchend', (event: any) => {
			this.onTouchEnd();
		}, {passive: true});
	}

	/**
	 * Closes the player from the lightbox experience.
	 */
	closePlayer() {
		this.player.pause();
		document.body.classList.toggle('lightbox-open', false);
		this.lightboxEl.classList.add('lightbox--closed');
		this.cards.forEach((card: { classList: { remove: (arg0: string) => void; }; }) => {
			card.classList.remove('hidden');
		});
	}

	/**
	 * Handles onTouchStart events.
	 * @param {!Event} event
	 */
	onTouchStart(event: {
		detail: {
			touches: {
				screenX: number; screenY: number;
			}[];
		};
	}) {
		this.lightboxEl.classList.add('dragging');
		this.touchStartX = event.detail.touches[0].screenX;
		this.touchStartY = event.detail.touches[0].screenY;
	}

	/**
	 * Handles onTouchMove events.
	 * @param {!Event} event
	 */
	onTouchMove(event: { detail: { touches: { screenX: any; screenY: any; }[]; }; }) {
		const { screenX, screenY } = event.detail.touches[0];

		if (this.isSwipeY === null) {
			this.isSwipeY =
				Math.abs(this.touchStartY - screenY) > Math.abs(this.touchStartX - screenX);
		}

		if (this.isSwipeY === false) {
			return;
		}

		this.deltaY = this.touchStartY - screenY;

		if (this.deltaY > 0) {
			// Swiping up.
			return;
		}

		if (!this.scalingDown) {
			// Set flag so loop doesn't kick off again while it's running.
			this.scalingDown = true;
			// Start of animate scale.
			this.animateScale(0);
		}

		this.isSwipeY = true;
		this.lightboxEl.style.transform = `translate3d(0, ${Math.pow(
			-this.deltaY,
			0.6
		)}px, 0) scale3d(${this.scaleVal}, ${this.scaleVal}, 1)`;
	}

	/**
	 * Handles onTouchEnd events.
	 */
	onTouchEnd() {
		this.resetAnimationScale();

		this.lightboxEl.classList.remove('dragging');
		if (this.isSwipeY === true && Math.abs(this.deltaY) > this.toggleThresholdPx) {
			this.closePlayer();
		} else if (this.isSwipeY === true) {
			this.resetStyles();
		}
		this.isSwipeY = null;
	}

	/**
	 * Animates scale for swipe down.
	 * @param {number} val
	 */
	animateScale(val: number) {
		if (val < 1 && this.scalingDown) {
			this.scaleVal = lerp(easeOutQuad(val), 1, 0.95);
			this.lightboxEl.style.transform = `translate3d(0px, ${Math.pow(
				-this.deltaY,
				0.6
			)}px, 0) scale3d(${this.scaleVal}, ${this.scaleVal}, 1)`;
			requestAnimationFrame(() => this.animateScale((val += 0.05)));
		}
	}

	/**
	 * Resets animation scale.
	 */
	resetAnimationScale() {
		this.scalingDown = false;
		this.scaleVal = 1;
	}

	/**
	 * Resets styles of the lightbox animation.
	 */
	resetStyles() {
		this.lightboxEl.style.transform = `translate3d(0, 0, 0) scale3d(1, 1, 1)`;
	}
	initializeCards() {
		this.setCards(this.doc.querySelectorAll('.story-carousel__card'));
		this.setCardMargin(parseFloat(getComputedStyle(this.cards[0]).marginRight))
		this.setCardWidth(this.cardMargin + this.cards[0].offsetWidth)

		const stories = this.player.getStories();

		this.cards.forEach((card: any, idx: any) => {
			card.addEventListener('click', () => {
				this.player.show(stories[idx].href, /* pageId */ null, { animate: false });
				document.body.classList.toggle('lightbox-open');
				this.lightboxEl.classList.remove('lightbox--closed');
				card.classList.add('hidden');

				this.resetStyles();
				this.player.play();
			}, {passive: true});

			card.addEventListener('mouseenter', () => {
				const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
				if (isMobile) return;
				this.showBackgroundCards(idx);
			}, {passive: true});

			card.addEventListener('mouseleave', () => {
				const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
				if (isMobile) return;
				this.hideBackgroundCards(idx);
			}, {passive: true});

			card.addEventListener('touchstart', () => {
				const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
				if (!isMobile) return;
				card.classList.toggle('story-carousel__card--touching');
				this.showBackgroundCards(idx);
			}, {passive: true});

			card.addEventListener('touchcancel', () => {
				const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
				if (!isMobile) return;
				card.classList.toggle('story-carousel__card--touching', false);
				this.hideBackgroundCards(idx);
			}, {passive: true});

			card.addEventListener('touchend', () => {
				const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
				if (!isMobile) return;
				card.classList.toggle('story-carousel__card--touching', false);
				this.hideBackgroundCards(idx);
			}, {passive: true});
		});
	}

	/**
	 * Displays background cards on hover and pushes next cards.
	 */
	showBackgroundCards(idx: any) {
		for (let i = idx + 1; i < this.cards.length; i++) {
			const savedTransform = this.cards[i].style.transform;
			const translateX = parseFloat(savedTransform.replace(/[^-?\d.]/g, '')) || 0;

			this.cards[i].style['transform'] = `translateX(${translateX + 24}px)`;
		}
	}

	/**
	 * Hides background cards and resets next siblings to original position.
	 */
	hideBackgroundCards(idx: any) {
		for (let i = idx + 1; i < this.cards.length; i++) {
			const savedTransform = this.cards[i].style.transform;
			const translateX = parseFloat(savedTransform.replace(/[^-?\d.]/g, '')) || 0;

			this.cards[i].style['transform'] = `translateX(${translateX - 24}px)`;
		}
	}

}

