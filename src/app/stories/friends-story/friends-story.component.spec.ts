import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendsStoryComponent } from './friends-story.component';

describe('FriendsStoryComponent', () => {
  let component: FriendsStoryComponent;
  let fixture: ComponentFixture<FriendsStoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FriendsStoryComponent]
    });
    fixture = TestBed.createComponent(FriendsStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
