import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreMemoriesStoryComponent } from './core-memories-story.component';

describe('CoreMemoriesStoryComponent', () => {
  let component: CoreMemoriesStoryComponent;
  let fixture: ComponentFixture<CoreMemoriesStoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CoreMemoriesStoryComponent]
    });
    fixture = TestBed.createComponent(CoreMemoriesStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
