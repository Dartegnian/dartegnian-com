import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentYearStoryComponent } from './current-year-story.component';

describe('CurrentYearStoryComponent', () => {
  let component: CurrentYearStoryComponent;
  let fixture: ComponentFixture<CurrentYearStoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CurrentYearStoryComponent]
    });
    fixture = TestBed.createComponent(CurrentYearStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
