import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicLikesStoryComponent } from './music-likes-story.component';

describe('MusicLikesStoryComponent', () => {
  let component: MusicLikesStoryComponent;
  let fixture: ComponentFixture<MusicLikesStoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MusicLikesStoryComponent]
    });
    fixture = TestBed.createComponent(MusicLikesStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
