import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BestPhotosStoryComponent } from './best-photos-story.component';

describe('BestPhotosStoryComponent', () => {
  let component: BestPhotosStoryComponent;
  let fixture: ComponentFixture<BestPhotosStoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BestPhotosStoryComponent]
    });
    fixture = TestBed.createComponent(BestPhotosStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
