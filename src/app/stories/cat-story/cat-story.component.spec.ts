import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatStoryComponent } from './cat-story.component';

describe('CatStoryComponent', () => {
  let component: CatStoryComponent;
  let fixture: ComponentFixture<CatStoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CatStoryComponent]
    });
    fixture = TestBed.createComponent(CatStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
