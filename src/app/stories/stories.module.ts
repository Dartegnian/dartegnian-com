import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { MusicLikesStoryComponent } from './music-likes-story/music-likes-story.component';
import { FriendsStoryComponent } from './friends-story/friends-story.component';
import { CurrentYearStoryComponent } from './current-year-story/current-year-story.component';
import { CatStoryComponent } from './cat-story/cat-story.component';
import { PlacesStoryComponent } from './places-story/places-story.component';
import { CoreMemoriesStoryComponent } from './core-memories-story/core-memories-story.component';
import { BestPhotosStoryComponent } from './best-photos-story/best-photos-story.component';
import { StoriesRoutingModule } from './stories-routing.module';

@NgModule({
	declarations: [
		MusicLikesStoryComponent,
		FriendsStoryComponent,
		CurrentYearStoryComponent,
		CatStoryComponent,
		PlacesStoryComponent,
		CoreMemoriesStoryComponent,
		BestPhotosStoryComponent,
	],
	imports: [StoriesRoutingModule],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	providers: [],
	exports: [],
})
export class StoriesModule { }
