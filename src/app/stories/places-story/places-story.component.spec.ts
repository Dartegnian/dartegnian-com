import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacesStoryComponent } from './places-story.component';

describe('PlacesStoryComponent', () => {
  let component: PlacesStoryComponent;
  let fixture: ComponentFixture<PlacesStoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlacesStoryComponent]
    });
    fixture = TestBed.createComponent(PlacesStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
