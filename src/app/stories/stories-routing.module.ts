import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmpStoryWrapperComponent } from '../components/amp-story-wrapper/amp-story-wrapper.component';
import { MusicLikesStoryComponent } from './music-likes-story/music-likes-story.component';
import { FriendsStoryComponent } from './friends-story/friends-story.component';
import { CurrentYearStoryComponent } from './current-year-story/current-year-story.component';
import { CatStoryComponent } from './cat-story/cat-story.component';
import { PlacesStoryComponent } from './places-story/places-story.component';
import { CoreMemoriesStoryComponent } from './core-memories-story/core-memories-story.component';
import { BestPhotosStoryComponent } from './best-photos-story/best-photos-story.component';

const routes: Routes = [
	{
		path: "stories",
		redirectTo: "/",
		pathMatch: 'full'
	},
	{
		path: "stories",
		component: AmpStoryWrapperComponent,
		children: [
			{ path: '', pathMatch: 'full', redirectTo: '/' },
			{ path: 'music', component: MusicLikesStoryComponent },
			{ path: 'friends', component: FriendsStoryComponent },
			{ path: 'current-year', component: CurrentYearStoryComponent },
			{ path: 'cat', component: CatStoryComponent },
			{ path: 'places', component: PlacesStoryComponent },
			{ path: 'core-memories', component: CoreMemoriesStoryComponent },
			{ path: 'best-photos', component: BestPhotosStoryComponent },
		]
	},
];

@NgModule({
	imports: [
		RouterModule.forChild(routes),
	],
	exports: [RouterModule]
})
export class StoriesRoutingModule { }
