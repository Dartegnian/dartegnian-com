import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { AccentService } from '@services/accent-service.service';
import { IdbService } from '@services/idb.service';
import { UpdateService } from '@services/update.service';
import { inject } from '@vercel/analytics';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	isBrowser: boolean = false;
	isCalendarModalClosed: boolean = false;

	constructor(
		private idb: IdbService,
		private accent: AccentService,
		private sw: UpdateService,
		@Inject(PLATFORM_ID) private platformId: Object,
		@Inject(DOCUMENT) private document: Document
	) {
		this.addStylesToHeader();
		this.isBrowser = isPlatformBrowser(this.platformId);
		this.sw.checkForUpdates();
		inject();
	}

	async ngOnInit() {
		if (this.isBrowser) {
			this.idb.connectToIDB();
			const customImage = await this.idb.getData("Material You", "customImage");

			if (customImage) {
				this.accent.setCustomImage(customImage, true);
			}

			const accentIndex = (await this.idb.getData("Material You", "themeIndex")) || 1;

			if (accentIndex !== "1") {
				this.accent.setAccent(accentIndex);
			}
		}
	}

	addStylesToHeader(): void {
		const materialSymbols = this.document.createElement('link');
		materialSymbols.href = "https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@48,700,0,0&display=swap";
		materialSymbols.crossOrigin = "";
		materialSymbols.rel = "stylesheet";
		this.document.head.appendChild(materialSymbols);
	}

	setIsModalOpen(isOpen: boolean) {
		this.isCalendarModalClosed = isOpen;
	}
}
