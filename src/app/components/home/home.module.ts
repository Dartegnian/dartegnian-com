import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ProfileCardComponent } from '@components/profile-card/profile-card.component';
import { SurfaceTestComponent } from '@components/surface-test/surface-test.component';
import { ThemeSwitcherComponent } from '@components/theme-switcher/theme-switcher.component';
import { ProfileInfoComponent } from '@components/profile-info/profile-info.component';
import { HeroBannerComponent } from '@components/hero-banner/hero-banner.component';
import { AccentSwitcherComponent } from '@components/accent-switcher/accent-switcher.component';
import { SkillPictureComponent } from '@components/skill-picture/skill-picture.component';
import { SkillInfoComponent } from '@components/skill-info/skill-info.component';
import { LifeAtAGlanceComponent } from '@components/life-at-a-glance/life-at-a-glance.component';
import { SkillListComponent } from '@components/skill-list/skill-list.component';
import { CommonModule } from '@angular/common';
import { EmailCtaComponent } from '@components/resume-request/resume-request.component';
import { FooterComponent } from '@components/footer/footer.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ResponsiveImageComponent } from '@components/responsive-image/responsive-image.component';
import { OtherSitesComponent } from '@components/other-sites/other-sites.component';
import { PersonalStatsComponent } from '@components/personal-stats/personal-stats.component';
import { PixelMapComponent } from '@components/pixel-map/pixel-map.component';
import { BlogFeedComponent } from '@components/blog-feed/blog-feed.component';

import { HttpClientModule } from '@angular/common/http';
import { AmpStoryWrapperComponent } from '@components/amp-story-wrapper/amp-story-wrapper.component';
import { HomeComponent } from '@components/home/home.component';
import { AmpStoryViewerComponent } from '@components/amp-story-viewer/amp-story-viewer.component';
import { HomeRoutingModule } from './home-routing.module';
import { SafeUrlPipe } from '@utils/pipes/safe-resource-url.pipe';

@NgModule({
  declarations: [
    ProfileCardComponent,
    SurfaceTestComponent,
    ThemeSwitcherComponent,
    ProfileInfoComponent,
    HeroBannerComponent,
    AccentSwitcherComponent,
    SkillPictureComponent,
    SkillInfoComponent,
    LifeAtAGlanceComponent,
    SkillListComponent,
    EmailCtaComponent,
    FooterComponent,
    ResponsiveImageComponent,
	OtherSitesComponent,
	PersonalStatsComponent,
	PixelMapComponent,
	BlogFeedComponent,
	AmpStoryWrapperComponent,
	HomeComponent,
	AmpStoryViewerComponent,
	HomeComponent,
	SafeUrlPipe
  ],
  imports: [
	HttpClientModule,
    BrowserModule,
	CommonModule,
	HomeRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  bootstrap: []
})
export class HomeModule { }
