import { Component, Renderer2, ViewEncapsulation, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
	selector: 'app-amp-story',
	templateUrl: './amp-story-wrapper.component.html',
	styleUrls: ['./amp-story-wrapper.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class AmpStoryWrapperComponent {
	ampScripts = [
		"https://cdn.ampproject.org/v0.js",
		"https://cdn.ampproject.org/v0/amp-video-0.1.js",
		"https://cdn.ampproject.org/v0/amp-story-1.0.js"
	];

	constructor(
		@Inject(DOCUMENT) private document: Document
	) {
		this.removeUnusedScripts();

		const mainStyle = Array.from(this.document.getElementsByTagName('link')).find(style =>
			style.href.match(/styles.*css/)
		);
		if (mainStyle) {
			mainStyle.remove();
		}
		this.ampScripts.forEach(
			(script) => {
				this.addScriptToHeader(script);
			}
		);
	}

	addScriptToHeader(url: string, callback?: () => void): void {
		const script = this.document.createElement('script');
		script.src = url;
		script.async = true;
		this.document.head.appendChild(script);
	}

	removeUnusedScripts(): void {
		const mainScript = Array.from(this.document.getElementsByTagName('script')).find(script =>
			script.src.match(/main.*js/)
		);
		if (mainScript) {
			mainScript.remove();
		}

		const runtimeScript = Array.from(this.document.getElementsByTagName('script')).find(script =>
			script.src.match(/runtime.*js/)
		);
		if (runtimeScript) {
			runtimeScript.remove();
		}

		const polyfillsScript = Array.from(this.document.getElementsByTagName('script')).find(script =>
			script.src.match(/polyfills.*js/)
		);
		if (polyfillsScript) {
			polyfillsScript.remove();
		}
	}
}
