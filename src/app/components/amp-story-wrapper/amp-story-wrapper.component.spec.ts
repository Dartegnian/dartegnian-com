import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmpStoryComponent } from './amp-story-wrapper.component';

describe('AmpStoryComponent', () => {
  let component: AmpStoryComponent;
  let fixture: ComponentFixture<AmpStoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AmpStoryComponent]
    });
    fixture = TestBed.createComponent(AmpStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
