import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, HostListener, Input, Output, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { calendarDays, calendarMonths } from '@data/pixel-map/pixel-map-calendar';
import IPixelMapDay from "@interfaces/pixel-map-day.interface";
import { firstValueFrom, map } from 'rxjs';
import * as albumArt from 'album-art';

@Component({
	selector: 'pixel-map',
	templateUrl: './pixel-map.component.html',
	styleUrls: ['./pixel-map.component.scss']
})
export class PixelMapComponent {
	calendar: any;
	legendData: any;

	activeMonth: number;
	activeDate: number;
	activeDayName: string;
	activeStatus: IPixelMapDay["status"] | undefined;
	isModalOpen: boolean;
	activeData: IPixelMapDay;
	moodIcons: any;
	spotifyImage: string = "";
	
	@Input() setModalCloseEvent = false;
	@Output() setModalEvent = new EventEmitter<boolean>();

	constructor(
		private http: HttpClient
	) {
		this.activeMonth = 0;
		this.activeDate = 1;
		this.activeDayName = "";
		this.isModalOpen = false;

		this.calendar = {
			days: [],
			months: calendarMonths,
			data: calendarDays
		};
		this.legendData = [
			{ class: "special", text: "Special" },
			{ class: "excellent", text: "Excellent" },
			{ class: "good", text: "Good" },
			{ class: "above-average", text: "Above Average" },
			{ class: "average", text: "Average" },
			{ class: "below-average", text: "Below Average" },
			{ class: "bad", text: "Bad" },
			{ class: "awful", text: "Awful" },
		];
		this.moodIcons = {
			"special": "add_reaction",
			"excellent": "sentiment_very_satisfied",
			"good": "mood",
			"above-average": "sentiment_satisfied",
			"average": "sentiment_neutral",
			"below-average": "sentiment_dissatisfied",
			"bad": "sentiment_very_dissatisfied",
			"awful": "sentiment_extremely_dissatisfied"
		};
		this.buildCalendar();
		this.activeData = this.calendar.data?.[0]?.[0];
	}

	ngOnChanges(changes: SimpleChanges) {
		for (const propName in changes) {
			const chng = changes[propName];
			const cur  = JSON.stringify(chng.currentValue);
			const prev = JSON.stringify(chng.previousValue);
			if (prev === "true" && cur === "false") {
				this.isModalOpen = false;
			}
		}
	}

	buildCalendar() {
		this.calendar.days = Array(31).fill(1).map((x, i) => i);
	}

	async setActiveDate(month: number, date: number, event: Event) {
		event.stopPropagation();
		const currentDate = new Date(Date.UTC(2023, month, date + 1));
		this.activeDayName = currentDate.toLocaleDateString('en-US', { weekday: 'short' });

		if (this.calendar.data?.[month]?.[date]) {
			this.activeData = this.calendar.data[month][date];

			if (this.activeData?.spotify) {
				await this.fetchAlbumArt(
					this.activeData.spotify.artist,
					this.activeData.spotify.album,
				);
			}

			this.activeStatus = this.activeData?.status;
			this.setIsModalOpen(true);
		} else {
			this.setIsModalOpen(false);
		}

		this.activeMonth = month;
		this.activeDate = date;
	}

	setIsModalOpen(isOpen: boolean, override?: boolean) {
		if (!isOpen && !override) {
			this.activeStatus = undefined;
			this.spotifyImage = "";
		}

		this.isModalOpen = isOpen;
		this.setModalEvent.emit(isOpen);
	}

	setStatus(status: IPixelMapDay["status"]) {
		if (status === this.activeStatus) {
			this.activeStatus = undefined;
		} else {
			this.activeStatus = status;
		}
		this.setIsModalOpen(false, true);
	}

	async fetchAlbumArt(
		artist: string,
		album: string
	) {
		const albumImage = await albumArt(`${artist}`, {album: `${album}`, size: 'medium'});
		const blobImage = await firstValueFrom(this.http.get(albumImage, { responseType: 'blob' }).pipe(map(response => response)));
		this.spotifyImage = URL.createObjectURL(blobImage);
	}

	@HostListener('document:keydown.escape')
	fn() {
		this.setIsModalOpen(false);
	}
}
