import { Component, OnInit } from '@angular/core';
import { musicData, featuredArtist } from '@data/music/music-data';
import * as albumArt from 'album-art';

@Component({
	selector: 'music-likes-info',
	templateUrl: './surface-test.component.html',
	styleUrls: ['./surface-test.component.scss']
})
export class SurfaceTestComponent implements OnInit {
	surfaces: Array<string> = [
		// "material-colored-secondary",
		// "material-container-primary",
		// "material-container-secondary",
		// "material-inverted-primary",

		// "material-colored-tertiary",

		"material-container-secondary",
		"material-colored-primary",
		"material-container-primary",
		"material-container-tertiary",

		"material-inverted-secondary",
		"material-inverted-tertiary",
	];
	largeSurfaces: Array<string> = [
		"material-colored-secondary",
		"material-container-tertiary"
	];
	wideSurfaces = [
		"material-inverted-primary",
		"material-inverted-secondary",
		"material-colored-tertiary"
	];
	artistSpotlight = featuredArtist;

	surfaceContent = musicData;
	shuffledArray: any[] = this.surfaceContent;

	constructor() {
		// this.shuffledArray = this.shuffleArray(this.surfaceContent);
	}

	ngOnInit(): void {
		this.parseArtistSpotlight();
	}

	shuffleArray(array: any[]) {
		// Thank you, StackOverflow
		// https://stackoverflow.com/questions/60787865/randomize-array-in-angular
		let m = array.length, t, i;

		while (m) {
			i = Math.floor(Math.random() * m--);
			t = array[m];
			array[m] = array[i];
			array[i] = t;
		}

		return array;
	}

	async fetchAlbumArt(
		artist: string,
		album?: string
	) {
		let albumImage = "";
		if (album) {
			albumImage = await albumArt(`${artist}`, {album: `${album}`, size: 'medium'});
		} else {
			albumImage = await albumArt(`${artist}`, {size: 'large'});
		}
		return albumImage;
	}

	async parseArtistSpotlight() {
		const artist = this.artistSpotlight.artist;
		this.artistSpotlight.image = await this.fetchAlbumArt(artist);

		this.artistSpotlight.albums.forEach(
			async (album, i) => {
				this.artistSpotlight.albums[i].image = await this.fetchAlbumArt(artist, album.title)
			}
		);
	}
}
