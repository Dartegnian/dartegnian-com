import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Parser from 'rss-parser';

@Component({
	selector: 'blog-feed',
	templateUrl: './blog-feed.component.html',
	styleUrls: ['./blog-feed.component.scss']
})
export class BlogFeedComponent {
	latestPost: any | undefined;
	feedItems: any[] = [];

	constructor(private http: HttpClient) { }

	ngOnInit() {
		this.fetchFeed();
	}

	fetchFeed() {
		const parser = new Parser();
		const url = 'https://blog.dartegnian.com/rss/';
		this.http.get(url, { responseType: 'text' }).subscribe((data) => {
			parser.parseString(data).then((result) => {
				result.items.every(
					(item, i) => {
						if (i > 3) {
							return false;
						}
						const regex = /<img[^>]+src="([^">]+)"/g;
						const match = regex.exec(item["content:encoded"]) || "";
						const imageUrl = match[1];
						item['imageUrl'] = imageUrl;
						item['imageUrl'] = item['imageUrl'].replace(/&amp;/g, '&');
						item['imageUrl'] = item['imageUrl'].replace(`fm=jpg`, 'fm=webp');
						item['date'] = new Date(item['pubDate'] as string).toLocaleDateString('en-US', {
							day: 'numeric',
							month: 'short',
							year: 'numeric',
						});

						if (i === 0 ) {
							this.latestPost = item;
							item['imageUrl'] = item['imageUrl'].replace(`q=80`, 'q=70');
							item['imageUrl'] = item['imageUrl'].replace(`w=2000`, 'w=900');

							if (item['imageUrl'].includes("/content/images/")) {
								item['imageUrl'] = item['imageUrl'].replace(`content/images/`, 'content/images/size/w1000/');
							}
						} else {
							item['imageUrl'] = item['imageUrl'].replace(`q=80`, 'q=45');
							item['imageUrl'] = item['imageUrl'].replace(`w=2000`, 'w=550');

							if (item['imageUrl'].includes("/content/images/")) {
								item['imageUrl'] = item['imageUrl'].replace(`content/images/`, 'content/images/size/w600/');
							}
							this.feedItems.push(item);
						}
						return true;
					}
				);
			});
		});
	}
}