import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmpStoryViewerComponent } from './amp-story-viewer.component';

describe('AmpStoryViewerComponent', () => {
  let component: AmpStoryViewerComponent;
  let fixture: ComponentFixture<AmpStoryViewerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AmpStoryViewerComponent]
    });
    fixture = TestBed.createComponent(AmpStoryViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
