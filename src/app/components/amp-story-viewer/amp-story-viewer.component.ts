import { AfterViewInit, Component, ElementRef, HostListener, ViewChild, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { AmpViewerService } from '@services/amp-viewer.service';
import storyCards from '@data/stories/viewer';

@Component({
	selector: 'app-amp-story-viewer',
	templateUrl: './amp-story-viewer.component.html',
	styleUrls: ['./amp-story-viewer.component.scss']
})
export class AmpStoryViewerComponent implements AfterViewInit {
	@ViewChild('ampStoryPlayer', { static: false })
	targetElementRef!: ElementRef;
	@ViewChild('storyPlayer', { static: false })
	storyElementRef!: ElementRef;
	storyCards: any[] = storyCards;

	constructor(
		private ampViewer: AmpViewerService,
		@Inject(DOCUMENT) private document: Document
	) {
		this.addScriptToHeader("https://cdn.ampproject.org/amp-story-player-v0.js");
	}

	addScriptToHeader(url: string): void {
		const script = this.document.createElement('script');
		script.src = url;
		script.async = false;
		this.document.head.appendChild(script);
	}

	shuffleArray(array: any[]) {
		// Thank you, StackOverflow
		// https://stackoverflow.com/questions/60787865/randomize-array-in-angular
		let m = array.length, t, i;

		while (m) {
			i = Math.floor(Math.random() * m--);
			t = array[m];
			array[m] = array[i];
			array[i] = t;
		}

		return array;
	}

	ngAfterViewInit() {
		const storyControls = {
			"behavior": {
				"pageScroll": false,
				"autoplay": false
			},
			"controls": [
				{
					"name": "close",
					"position": "start"
				},
				{
					"name": "skip-to-next"
				}
			]
		};
		const script = this.document.createElement('script');
		script.type = "application/json";
		script.innerHTML = JSON.stringify(storyControls);
		this.storyElementRef.nativeElement.appendChild(script);
		this.init();
	}

	/**
	 * Initializes the carousel once the player is ready.
	*/
	init() {
		const nativeElement = this.targetElementRef.nativeElement;
		this.ampViewer.setDocument(nativeElement);

		if (this.ampViewer.player.isReady) {
			this.initializeCarousel();
		} else {
			this.ampViewer.player.addEventListener('ready', () => {
				this.initializeCarousel();
			}, { passive: true });
		}
	}

	/**
	 * Initializes cards, arrows, and listeners for the carousel.
	*/
	initializeCarousel() {
		const lightbox = this.targetElementRef.nativeElement.querySelector('.lightbox');
		this.ampViewer.setLightbox(lightbox);

		this.ampViewer.initializeCards();
		this.ampViewer.player.addEventListener('amp-story-player-close', () => {
			this.closePlayer();
		}, { passive: true });

		// For swipe down to close.
		this.ampViewer.initializeTouchListeners();
	}

	/**
	 * Closes the player from the lightbox experience.
	*/
	closePlayer() {
		this.ampViewer.player.pause();
		document.body.classList.toggle('lightbox-open', false);
		this.ampViewer.lightboxEl.classList.add('lightbox--closed');
		this.ampViewer.cards.forEach((card: any) => {
			card.classList.remove('hidden');
		});
	}

	@HostListener('document:keydown.escape')
	fn() {
		this.closePlayer();
	}
}
