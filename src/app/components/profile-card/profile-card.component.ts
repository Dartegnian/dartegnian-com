import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AccentService } from 'src/app/services/accent-service.service';

@Component({
	selector: 'profile-card',
	templateUrl: './profile-card.component.html',
	styleUrls: ['./profile-card.component.scss']
})
export class ProfileCardComponent implements OnInit {
	name = "Dartegnian L. Velarde";
	tagline = "a lone star in the sky.";
	website = "https://blog.dartegnian.com";
	email = "contact@dartegnian.com";
	adjectives: any[] = [
		{
			icon: "edit",
			name: "Writer"
		},
		{
			icon: "crisis_alert",
			name: "Archer"
		},
		{
			icon: "terminal",
			name: "DevOps Engineer",
			itemProp: "jobTitle"
		},
		{
			icon: "queue_music",
			name: "K-Pop stan"
		}
	];
	shuffledArray: any[];

	images: Array<string>;
	coverImage: string;
	secondCoverImage: string | undefined;
	accentSubscription: Subscription;
	isSecondCoverImageActive = false;
	activeIndex: number;
	customImage: string | ArrayBuffer | null = null;

	constructor(
		private accent: AccentService
	) {
		this.shuffledArray = this.shuffleArray(this.adjectives);
		this.images = this.accent.images;
		this.coverImage = this.images[this.accent.activeIndex];
		this.activeIndex = Number(this.accent.activeIndex);
		this.customImage = this.accent.customImage;

		this.accentSubscription = this.accent.accentSubscription.subscribe(
			(index: number) => {
				this.setCoverImage(index);
				this.customImage = this.accent.customImage;
			}
		);
	}

	ngOnInit(): void {
	}

	setCoverImage(index: number) {
		if (this.activeIndex !== index) {
			if (this.isSecondCoverImageActive) {
				this.coverImage = index === 0 ? this.images[this.activeIndex] : this.images[index];
				this.isSecondCoverImageActive = false;
			} else {
				this.secondCoverImage = index === 0 ? this.images[this.activeIndex] as string : this.images[index];
				this.isSecondCoverImageActive = true;
			}
		}

		this.activeIndex = index;
	}
	
	shuffleArray(array: any[]) {
		// Thank you, StackOverflow
		// https://stackoverflow.com/questions/60787865/randomize-array-in-angular
		let m = array.length, t, i;

		while (m) {
			i = Math.floor(Math.random() * m--);
			t = array[m];
			array[m] = array[i];
			array[i] = t;
		}

		return array;
	}
}
