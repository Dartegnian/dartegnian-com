/**
 * Linear interpolation function for animation.
 * @param {number} pct
 * @param {number} v0
 * @param {number} v1
 * @return {number}
 */
export function lerp(pct: number, v0: number, v1: number) {
	return v0 * (1 - pct) + v1 * pct;
}

/**
 * Ease out quad for animation.
 * @param {number} t
 * @return {number}
 */
export function easeOutQuad(t: number) {
	return --t * t * t + 1;
}
